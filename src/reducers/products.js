const initialState = {
    isLoaded: false,
    items: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SET_PRODUCT':
            return {
                ...state,
                items: action.payload,
                isLoaded: true
            }
        case 'SET_IS_LOADED':
            return {
                ...state,
                isLoaded: action.payload
            }
        default:
            return state;
    }
}