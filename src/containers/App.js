import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as productActions from '../actions/products'
import App from '../components/App'


const mapStateToProps = ({products}) => ({
    products: products.items,
    isLoaded: products.isLoaded
  })
  
  const mapDispatchToProps = dispatch => ({
      ...bindActionCreators(productActions, dispatch),
  })
  
  export default connect(mapStateToProps, mapDispatchToProps)(App);