import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as cartActions from "../actions/cart";
import Checkout from "../components/CheckoutPage";


const mapStateToProps = ({products, cart}) => ({
    cart: cart.items,
    total: cart.items.reduce((total, product) => total + product.price, 0 ),
  })
  
  const mapDispatchToProps = dispatch => ({
      ...bindActionCreators(cartActions, dispatch),
  })
  
  export default connect(mapStateToProps, mapDispatchToProps)(Checkout);