import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as cartActions from "../actions/cart";
import Card from "../components/card";


const mapStateToProps = ({cart}) => ({
    addedCount: 0
  })
  
  const mapDispatchToProps = dispatch => ({
      ...bindActionCreators(cartActions, dispatch),
  })
  
  export default connect(mapStateToProps, mapDispatchToProps)(Card);