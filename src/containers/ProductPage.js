import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as cartActions from "../actions/cart";
import ProductPage from '../components/ProductPage'


const mapStateToProps = ({products}) => ({
    products: products.items,
    isLoaded: products.isLoaded,
  })
  
  const mapDispatchToProps = dispatch => ({
      ...bindActionCreators(cartActions, dispatch),
  })
  
  export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);