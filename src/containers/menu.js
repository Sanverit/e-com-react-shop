import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as cartActions from "../actions/cart";
import Menu from "../components/menu";


const mapStateToProps = ({cart}) => ({
    total: cart.items.reduce((total, product) => total + product.price, 0 ),
    count: cart.items.length,
    items: cart.items
  })
  
  const mapDispatchToProps = dispatch => ({
      ...bindActionCreators(cartActions, dispatch),
  })
  
  export default connect(mapStateToProps, mapDispatchToProps)(Menu);