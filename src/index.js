import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from 'react-redux';
import createStore from './store'

import 'semantic-ui-css/semantic.min.css'
import './App.css'
import App from './containers/App';
import ProductPage from './containers/ProductPage';
import CheckoutPage from './containers/CheckoutPage';

const store = createStore();

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route path={'/'} exact component={App}/>
                <Route path={'/product/:id'} exact component={ProductPage}/>
                <Route path={'/checkout'} exact component={CheckoutPage}/>
            </Switch>
        </BrowserRouter>
    </Provider>
, document.getElementById('root'));
