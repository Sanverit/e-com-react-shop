import React, {Component} from 'react';
import axios from 'axios'

import { Container, Card } from "semantic-ui-react";
import ProductCard from "../containers/card";
import Menu from "../containers/menu";
import '../App.css';



class App extends Component {
  componentWillMount(){
    const {setProducts} = this.props;
    axios.get('./products.json').then(response => {
      console.log('response', response);
      setProducts(response.data);
    })
  }

  render(){
    const {products, isLoaded} = this.props
    return (
        <Container>
          <Menu />
          <Card.Group itemsPerRow={4}>
            {
              !isLoaded ? 'Loading...' : products.map(product => (
                  <ProductCard key={product.id} {...product} />
              ))
            }
          </Card.Group>
        </Container >
    )
  }
}

export default App;
