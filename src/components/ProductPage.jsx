import React, {Component} from 'react';
import { useParams } from "react-router-dom";
import { Container, Image, Segment, Grid, Item, Icon, Button } from "semantic-ui-react";
import Menu from "../containers/menu";

const ProductPage = ({products, addToCart, removeFromCart}) => {
  const { id } = useParams();
  let product = products.find(obj => obj.id == id);
  let productToCart = {...product, addToCart, removeFromCart };
  return(
    <Container>
      <Menu />
      <Segment>
        <Grid columns={2} relaxed='very'>
          <Grid.Column>
            <Image src={product.image} wrapped />
          </Grid.Column>
          <Grid.Column>
            <Item.Group>
              <Item>
                <Item.Content>
                  <Item.Header>{product.title}</Item.Header>
                  <Item.Meta>
                    <span className='price'>Price: {product.price} <Icon name='euro sign' /></span>
                  </Item.Meta>
                  <Item.Meta>
                    <span className='stay'>{product.isAvalible ? 'in stock' : 'absent'}</span>
                  </Item.Meta>
                  <Item.Meta>
                    <span className='stay'>SKU: {product.sku}</span>
                  </Item.Meta>
                  <Item.Description>{product.description}</Item.Description>
                </Item.Content>
              </Item>
              <Button fluid disabled={!product.isAvalible} onClick={() => addToCart(productToCart)} color="green" >Add to cart</Button>
            </Item.Group>
          </Grid.Column>
        </Grid>
      </Segment>
    </Container>
    
  )
}

export default ProductPage;
