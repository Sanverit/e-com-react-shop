import React from 'react'
import {Link} from 'react-router-dom'
import { Menu, Popup, List, Button, Image } from 'semantic-ui-react'

const CartComponent = ({title, id, image, removeFromCart }) => (
    <List divided verticalAlign='middle' size="mini">
        <List.Item>
        <List.Content floated='right'>
            <Button size='mini' color="red" onClick={() => removeFromCart(id)}>X</Button>
        </List.Content>
        <Image avatar src={image} />
        <List.Content>{title}</List.Content>
        </List.Item>
    </List>
) 

const MenuTop = ({total, count, items}) => (
    <Menu>
        <Menu.Item
            as={Link} 
            to="/"
            name='name'
        >
            Products Store
        </Menu.Item>
        <Menu.Menu position='right'>
            <Menu.Item
            name='total'
            >
            Total: <b>{total}</b>
            </Menu.Item>

            <Popup
                trigger={
                    <Menu.Item  
                        name='cart'
                        >
                        Cart: <b>{count}</b>
                    </Menu.Item>
                }
                content={ 
                        <div>
                            { items.map((product, id) => <CartComponent key={id} {...product}/>) }
                            <Button fluid color="green" as={Link} to='/checkout'>Check out</Button>
                        </div>
                }
                on="click"                
                hideOnScroll
            >
            </Popup>
        </Menu.Menu>
    </Menu>
);

export default MenuTop;