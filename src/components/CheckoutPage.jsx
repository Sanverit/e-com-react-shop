import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import { Container, Form, Image, Table, List, Modal, Button } from "semantic-ui-react";
import axios from 'axios'
import Menu from "../containers/menu";

const PopupContent = ({cart}) => {

  return (
    <Table color='green' key='green'>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Image</Table.HeaderCell>
          <Table.HeaderCell>Title</Table.HeaderCell>
          <Table.HeaderCell>Price</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {cart.map((item) => (
          <Table.Row>
            <Table.Cell><Image avatar src={item.image} /></Table.Cell>
            <Table.Cell>{item.title}</Table.Cell>
            <Table.Cell>{item.price}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  )
}


export default class CheckoutPage extends Component {

  constructor(props) {
    super(props)

    this.state = {
      sendTo: '',
      fattura: '',
      cart: '',
      isSending: false,
      popupOpen: false
    }
  }

  postData () {
    const {sendTo, fattura, cart} = this.state;
    this.setState({isSending: true});
    axios({
      method: 'post',
      url: 'http://postToServer.com',
      data: { sendTo, fattura, cart}
    })
    .then(function (response) {
      console.log('response', response);
    })
    .catch(
      function(error){
        this.setState({isSending: false, popupOpen: true});
      }.bind(this)
    );

  }

  render () {
    const { isSending } = this.state;
    const { cart } = this.props;
    return (
      <Container>
        <Menu />
        <Form>
          <Form.Field>
            <label>Indirizzo di spedizione</label>
            <input placeholder='' onChange={(e) => this.setState({sendTo: e.target.value})} />
          </Form.Field>
          <Form.Field>
            <label>Indirizzo di fatturazione</label>
            <input placeholder='' onChange={(e) => this.setState({fattura: e.target.value})} />
          </Form.Field>
          <Form.Field>
            <label>Dati Carta di credito</label>
            <input placeholder='' onChange={(e) => this.setState({cart: e.target.value})} />
          </Form.Field>
          { !isSending ? 
          <Button onClick={() => this.postData()} color="green" type='submit'>Submit</Button> 
          :
          <Button loading primary> Loading </Button>
          }
        </Form>
        <Modal
          open={this.state.popupOpen}
          header='Your order send:'
          content={<PopupContent cart={cart}/>}
          actions={[{ key: 'done', content: 'Ok', positive: true }]}
        />
      </Container >
    );
  }
  
}