import React from 'react'
import {Link} from 'react-router-dom'
import { Card, Icon, Image, Button } from 'semantic-ui-react'

const ProductCard = (product) => {
  const {title, image, price, addToCart, id} = product;
  return (
    <Card>
      <Image src={image} wrapped ui={false} />
      <Card.Content>
        <Card.Header>{title}</Card.Header>
      </Card.Content>
      <Card.Content extra>
        <a>
        Price: <b>{price}</b>
          <Icon name='euro sign' />
        </a>
      </Card.Content>
      <Button as={Link} to={`/product/${id}`} color="blue">Go to pdp</Button>
      <Button disabled={!product.isAvalible} color="green"  onClick={() => addToCart(product)}>Add to cart</Button>
    </Card>
  );
}


export default ProductCard